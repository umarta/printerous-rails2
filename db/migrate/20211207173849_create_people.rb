class CreatePeople < ActiveRecord::Migration[6.1]
  def change
    create_table :person do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :avatar
      t.string :organization_id

      t.timestamps
    end
  end
end
