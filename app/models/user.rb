class User < ApplicationRecord
  has_secure_password
  has_many :organizations

end
