class Person < ApplicationRecord
  self.table_name = 'person'
  mount_uploader :avatar, ImageUploader

end
