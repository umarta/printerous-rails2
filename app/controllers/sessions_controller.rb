class SessionsController < ApplicationController
  skip_before_action :authorized, only: [:new, :create, :welcome]

  def new
    @user = User.new

  end

  def create
    @user = User.find_by(username: params[:username])
    if @user && @user.authenticate(params[:password])
      session[:user_id] = @user.id
      redirect_to welcome_path
    else
      redirect_to login_path
    end

  end

  def login
  end

  def welcome
  end

  def page_requires_login
  end

  def destroy
    User.find(session[:user_id])
    session[:user_id] = nil
    redirect_to welcome_path
  end

end
