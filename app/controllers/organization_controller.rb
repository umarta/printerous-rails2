class OrganizationController < ApplicationController
  def index
    @organization = Organization.all
  end

  def show
  end

  def new
    @new = Organization.new

  end

  def store
    @store = Organization.new(organization_params)

    if @store.save

      redirect_to organization_index_path, notice: 'Organization berhasil ditambahkan'
    else
      render :new
    end

  end

  def edit
    @edit = Organization.find(params[:id])

  end

  def update
    @update = Organization.find(params[:id])
    if @update.update(organization_params)
      redirect_to organization_index_path, notice: 'Organization berhasil diupdate'
    else
      render :edit
    end
  end

  def delete
    @delete = Organization.find(params[:id])
    if @delete.destroy
      redirect_to organization_index_path(params[:id]), notice: 'Organization berhasil dihapus'
    else
      render person_index_path(params[:organization_id])
    end
  end

  private

  def organization_params
    params.require(:organization).permit(:name, :phone, :email, :website, :logo)
  end

end

