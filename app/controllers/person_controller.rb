class PersonController < ApplicationController
  def index
    @person = Person.where(organization_id: params[:id])
  end

  def edit
    @edit = Person.find(params[:id])
  end

  def update
    @update = Person.find(params[:id])
    if @update.update(person_params)
      redirect_to person_index_path(params[:organization_id]), notice: 'Person berhasil diupdate'
    else
      render :edit

    end
  end

  def new
    @new = Person.new

  end

  def store
    @store = Person.new(person_params)

    if @store.save

      redirect_to person_index_path, notice: 'Person berhasil ditambahkan'
    else
      render :new
    end
  end

  def delete
    @delete = Person.find(params[:id])
    if @delete.destroy
      redirect_to person_index_path(params[:organization_id]), notice: 'Person berhasil dihapus'
    else
      render person_index_path(params[:organization_id])
    end
  end

  def person_params
    params.require(:person).permit(:name, :phone, :email, :avatar, :organization_id)
  end

end
