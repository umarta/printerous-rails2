class UsersController < ApplicationController
  skip_before_action :authorized, only: [:new, :create]

  def new
    @user = User.new
  end

  def create
    @user = User.create(params.require(:user).permit(:username,
                                                     :password))
    session[:user_id] = @user.id
    redirect_to welcome_path

  end

  def edit
    @edit = User.find(params[:id])
    @organization = Organization.all
  end

  def update
    @update = User.find(params[:id])
    if @update.update(user_params)
      redirect_to users_path
    end
  end

  def index
    @user = User.joins("left join organizations on users.account_manager = organizations.id").select(:'id', :'username', :'role', :'organizations.name')
  end

  def user_params
    params.require(:user).permit(:username, :role, :account_manager)
  end

end
