Rails.application.routes.draw do
  resources :users, only: [:new, :create,:index,:update]
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create', as: 'post_login'
  patch 'users/update/:id', to: 'users#update', as: 'user_update'
  get 'users/edit/:id', to: 'users#edit', as: 'user_edit'
  root to: 'sessions#welcome', as: 'welcome'

  get 'organization', to: 'organization#index', as: 'organization_index'
  get 'organization/show', to: 'organization#show', as: 'organization_show'
  get 'organization/new', to: 'organization#new', as: 'organization_new'
  post 'organization/store', to: 'organization#store', as: 'organization_store'
  get 'organization/:id/edit', to: 'organization#edit', as: 'organization_edit'
  patch 'organization/:id/update', to: 'organization#update', as: 'organization_update'
  delete 'organization/:id/delete', to: 'organization#delete', as: 'organization_delete'

  get 'organization/:id/person', to: 'person#index', as: 'person_index'
  get 'organization/:id/person/new', to: 'person#new', as: 'person_new'
  post 'organization/:id/person/store', to: 'person#store', as: 'person_store'
  get 'organization/:organization_id/person/edit/:id', to: 'person#edit', as: 'person_edit'
  patch 'organization/:organization_id/person/edit/:id', to: 'person#update', as: 'person_update'
  delete 'organization/:organization_id/person/delete/:id', to: 'person#delete', as: 'person_delete'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get 'authorized', to: 'sessions#page_requires_login'
  delete '/logout' => 'sessions#destroy'

end
